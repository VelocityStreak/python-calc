import math
import time as t

class start:
    def sin(number):
        answer = math.sin(number)
        print(f"Your answer is {answer}")

    def cos(number):
        answer = math.cos(number)
        print(f"Your answer is {answer}")

    def tan(number):
        answer = math.tan(number)
        print(f"Your answer is {answer}")

    def add(num1, num2):
        answer = num1 + num2
        print(f"Your answer is {answer}")

    def sub(num1, num2):
        answer = num1 - num2
        print(f"Your answer is {answer}")

    def multi(num1, num2):
        answer = num1 * num2
        print(f"Your answer is {answer}")

    def div(num1, num2):
        answer = num1 / num2
        print(f"Your answer is {answer}")

    def init():
        print("Please choose from the following:\nTrig:\t\t[sin, cos, tan]\nStandard:\t[add, subtract, divide, multiply]")
        selection = str(input()).lower()
        print(selection)
                
        if selection == "sin":
            print("Enter number to sin:\t")
            userNumber = float(input())
            start.sin(userNumber)
            return

        if selection == "cos":
            print("Enter number to cos:\t")
            userNumber = float(input())
            start.cos(userNumber)
            return

        if selection == "tan":
            print("Enter number to tan:\t")
            userNumber = float(input())
            start.tan(userNumber)
            return

        if selection == "add":
            print("Enter first number:\t")
            userNumber1 = float(input())
            print("Enter second number:\t")
            userNumber2 = float(input())
            start.add(userNumber1, userNumber2)
            return

        if selection == "subtract":
            print("Enter first number:\t")
            userNumber1 = float(input())
            print("Enter second number:\t")
            userNumber2 = float(input())
            start.sub(userNumber1, userNumber2)
            return

        if selection == "multiply":
            print("Enter first number:\t")
            userNumber1 = float(input())
            print("Enter second number:\t")
            userNumber2 = float(input())
            start.multi(userNumber1, userNumber2)
            return

        if selection == "divide":
            print("Enter first number:\t")
            userNumber1 = float(input())
            print("Enter second number:\t")
            userNumber2 = float(input())
            start.div(userNumber1, userNumber2)
            return

        else:
            print("invalid option!")
            return

while True: 
    print("Launch program? [y/n] -- Made by Velocity Streaks Q4 2021")
    qUser = str(input())

    if qUser == "y":
        start.init()

    else:
        print("quiting...")
        t.sleep(1)
        break

